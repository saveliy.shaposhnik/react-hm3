import React, { useState } from 'react'
import Items from '../../components/Items/Items';
import NoItems from '../../components/NoItems/NoItems';

export default function Favourite(props) {
    const { items, fillSVG, onClick } = props;
    const [isLikedOpen] = useState(true)
    const liked = JSON.parse(localStorage.getItem("Liked")) || []
    let likedItems = []

    const getLocalCart = () => {
        items.forEach(el => {
            liked.forEach(element => {
                if (element === el.id) {
                    likedItems.push(el);
                }
            });
        })
    }
    
    if(liked.length === 0){
        return <NoItems/>
    }

    return (
        <div className="cards-div">
            {getLocalCart()}
            {likedItems.map(el => {
                return <Items key={el.id} items={el} fillSVG={fillSVG} onClick={onClick} isLikedOpen={isLikedOpen}/>
            })}
        </div>
    )
}
