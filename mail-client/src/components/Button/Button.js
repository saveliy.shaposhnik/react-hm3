import "./Button.scss";
import React from 'react'

export default function Button(props) {
    const { text, backgroundColor, onClick, className } = props; 
    return (
        <>
            <button style={{ backgroundColor: backgroundColor }} className={className} onClick={onClick}>{text}</button>
        </>
    )
}


