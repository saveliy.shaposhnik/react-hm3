import Button from '../Button/Button';
import Icon from '../Icon/Icon';
import "./Items.scss"
import React from 'react'

export default function Items(props) {
    const { fillSVG, onClick, items, isCartOpen, isMainOpen,isLikedOpen } = props
    const { items: { isFavorite, Url, title, id, price } } = props;
    return (
        <div className="card" id={id}>
            <img src={Url} alt="Img" width="230px" height="230px" />
            <h3>{title}</h3>
            <div className="card-footer">
                <p>{price}$</p>
                {isMainOpen &&
                    <Icon
                        onClick={() => fillSVG(items)}
                        color="#ffd700"
                        filled={isFavorite ? "#ffd700" : "none"}
                    />
                }
                {isLikedOpen &&
                    <Icon
                        onClick={() => fillSVG(items)}
                        color="#ffd700"
                        filled={isFavorite ? "#ffd700" : "none"}
                    />
                }

                {isCartOpen && <Button text="X" backgroundColor="inherit" className="closeBtn" onClick={() => onClick(items)} />}
                {isMainOpen && <Button text="ADD TO CART" backgroundColor="black" className="openBtn" onClick={() => onClick(items)} />}
            </div>
        </div>
    )
}


