import "./Modal.scss";
import Button from "../Button/Button"
import React from 'react'

export default function Modal(props) {
    const { header, text, closeButton, onClick, addToCart } = props;
    return (
        <>
            <div className="darkGround" onClick={onClick}></div>
            <div className="modal">
                <header className="header">
                    {header}
                    {closeButton && <Button text="X" backgroundColor="inherit" onClick={onClick} className="closeBtn" />}
                </header>
                <main className="main">
                    {text}
                    <div className="btns">
                        <Button backgroundColor="#523637" text="Ok" className="btn" onClick={addToCart} />
                        <Button backgroundColor="#523637" text="Cancel" className="btn" onClick={onClick} />
                    </div>
                </main>
            </div>
        </>
    )
}


