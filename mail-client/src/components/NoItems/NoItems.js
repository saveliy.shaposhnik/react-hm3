import React from 'react'

export default function NoItems() {
    return (
        <h2>
            No Items
        </h2>
    )
}
