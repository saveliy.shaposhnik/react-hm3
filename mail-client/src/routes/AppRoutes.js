import React from 'react';
import { Redirect, Route, Switch } from "react-router-dom";
import Cart from '../pages/Cart/Cart';
import Main from '../pages/Main/Main';
import Favourites from "../pages/Favourite/Favourite"


export default function AppRoutes(props) {
    const {setItems,inLocal,fillSVG,onClick,items} = props;
    return (
        <div className="app-routes">
            <Switch>
                <Redirect exact from="/" to="/main" />
                <Route exact path="/main" render={() => <Main setItems={setItems} inLocal={inLocal} fillSVG={fillSVG} onClick={onClick} items={items}/>} />
                <Route exact path="/cart" render={() => <Cart setItems={setItems} inLocal={inLocal} fillSVG={fillSVG} onClick={onClick} items={items}/>} />
                <Route exact path="/favourites" render={() => <Favourites setItems={setItems} inLocal={inLocal} fillSVG={fillSVG} onClick={onClick} items={items}/>} />
                <Route path="*" render={() => <div>404</div>} />
            </Switch>
        </div>
    )
}
