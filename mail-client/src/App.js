import './App.css';
import Modal from './components/Modal/Modal';
import React, { useState,useEffect } from 'react'
import AppRoutes from './routes/AppRoutes';
import Sidebar from './components/Sidebar/Sidebar';


export default function App() {
  const [isActive, setIsActive] = useState(false);
  const [items, setItems] = useState([]);
  const [item, setItem] = useState([]);

  const localStorageSetCart = () => {
    let cartArr = JSON.parse(localStorage.getItem("Cart")) || [];

    if (cartArr.includes(item.id)) {
      const newCartArr = cartArr.filter(el => el !== item.id);
      cartArr = newCartArr;
    } else {
      cartArr.push(item.id)
    }

    const cart = JSON.stringify(cartArr)
    localStorage.setItem("Cart", cart);
  }

  const addToCart = () => {
    setIsActive(false);
    localStorageSetCart();
  }

  const inLocal = (items) => {
    const inLocal = JSON.parse(localStorage.getItem("Liked")) || [];
    const newArr = items.map(el => {
      if (inLocal.includes(el.id)) {
        el.isFavorite = !el.isFavorite
      }
      return el
    })
    setItems(newArr)
  }

  useEffect(() => {
    fetch("http://localhost:3000/items.json", {
      method: "GET",
    }).then((res) => res.json()).then((data) => {
      setItems(data);
      inLocal(data);
    });
  }, [])

  const openModal = (item) => {
    setItem(item);
    setIsActive(true);
  }

  const closeModal = () => {
    setIsActive(false);
  }

  const localStorageSetLiked = (item) => {
    let likedArr = JSON.parse(localStorage.getItem("Liked")) || [];

    if (likedArr.includes(item.id)) {
      const newlikedArr = likedArr.filter(el => el !== item.id);
      likedArr = newlikedArr;
    } else {
      likedArr.push(item.id)
    }

    const liked = JSON.stringify(likedArr)
    localStorage.setItem("Liked", liked);
  }

  const fillSVG = (item) => {
    const newArr = items.map(el => {
      if (el.id === item.id) {
        el.isFavorite = !el.isFavorite
      }
      return el
    })

    setItems(newArr)
    localStorageSetLiked(item)
  }
  return (
    <div className="App">
      <Sidebar/>
      <AppRoutes setItems={setItems} inLocal={inLocal} fillSVG={fillSVG} onClick={openModal} items={items}/>
      {isActive && <Modal header="Do you want to add this to cart?" text="" closeButton={true} onClick={closeModal} addToCart={addToCart} />}
    </div>
  )
}